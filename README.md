# Royal Game of Ur Z88

Mark Bennett made this game is 2022, link below. I discovered there was a Z88 version of it at the weekend. Quite rare to see games being released for the Z88.

[Original game](https://github.com/mark-bennett-uk/ur/)

BBC Basic for the Z88 allows the use of UDG. My version of the game update the graphics to use Z88 specific VDU/ASC codes. 

These codes can be used to create menus, sounds and UDGs on the Z88.

[BBC key words for the Z88](https://cambridgez88.jira.com/wiki/spaces/UG40/pages/14024771/BBC+BASIC+Keywords)

and 

[Output to the screen driver](https://cambridgez88.jira.com/wiki/spaces/DN/pages/2392073/Output+and+the+screen+driver)


## Installation

This listing can be 'loaded' by CLI. Copy this source code as save as text file as 'URZ88.BBC' on your desktop, then upload to :RAM.0 on your Z88. Start BBC BASIC application, execute command *CLI .*:RAM.0/URZ88.BBC to "type it in". Finally, SAVE/RUN.


## Support
gimmie a shout

## Roadmap

I'd like the add the border and some pauses and sounds to enhance the gameplay

## Authors and acknowledgment
Thanks to Mark Bennett who made the game and the code avilible. He did all the hard work.

## License
For open source projects, say how it is licensed.

## Project status
Active.
